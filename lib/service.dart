import 'package:flutter/material.dart';

import 'package:survive_now/profile.dart';
import 'package:survive_now/dashboard.dart';
import 'package:survive_now/schedule.dart';

class Service_page extends StatelessWidget {
  const Service_page({super.key});

  @override
  Widget build(BuildContext context) {
    double h1 = MediaQuery.of(context).size.height * .2;
    double h3 = MediaQuery.of(context).size.height * .45;
    double h2 = MediaQuery.of(context).size.height * .1;
    double tittle = MediaQuery.of(context).size.height * .05;
    double text = MediaQuery.of(context).size.height * .03;
    Color textc = Colors.green;
    Color backc = Color.fromARGB(255, 242, 240, 236);
    return Scaffold(
      backgroundColor: backc,
      body: Column(
        children: [
          Spacer(),
          Icon(
            Icons.sports_kabaddi,
            size: tittle,
          ),
          Text(
            'Choose Instructor',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: tittle,
              color: Colors.green,
            ),
          ),
          Spacer(),
          Container(
            height: MediaQuery.of(context).size.height * .75,
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    child: Column(
                      children: [
                        Spacer(),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            image: DecorationImage(
                                image: AssetImage('assets/franger.jpg'),
                                fit: BoxFit.cover),
                          ),
                          height: h1,
                          width: h1,
                        ),
                        Spacer(),
                        Text(
                          'Ranger Ryan Zoldyck',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: tittle,
                            color: backc,
                          ),
                        ),
                        Spacer(),
                        Text(
                          'SKills: Building Shelter, Starting Fire',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: text,
                            color: backc,
                          ),
                        ),
                        Spacer(),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.amber,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Schedule_page(),
                              ),
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 25, right: 25, top: 15, bottom: 15),
                            child: Text(
                              'Select',
                              style: TextStyle(
                                  fontSize: text,
                                  color: backc,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20), color: textc),
                    height: h3,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    child: Column(
                      children: [
                        Spacer(),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            image: DecorationImage(
                                image: AssetImage('assets/dranger.jpg'),
                                fit: BoxFit.cover),
                          ),
                          height: h1,
                          width: h1,
                        ),
                        Spacer(),
                        Text(
                          'Ranger Chrollo Lucifer',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: tittle,
                            color: backc,
                          ),
                        ),
                        Spacer(),
                        Text(
                          'SKills: Building Shelter,',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: text,
                            color: backc,
                          ),
                        ),
                        Spacer(),
                        Text(
                          'procuring food & drinkable water',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: text,
                            color: backc,
                          ),
                        ),
                        Spacer(),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.amber,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Schedule_page(),
                              ),
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 25, right: 25, top: 15, bottom: 15),
                            child: Text(
                              'Select',
                              style: TextStyle(
                                  fontSize: text,
                                  color: backc,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20), color: textc),
                    height: h3,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    child: Column(
                      children: [
                        Spacer(),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            image: DecorationImage(
                                image: AssetImage('assets/aranger.jpg'),
                                fit: BoxFit.cover),
                          ),
                          height: h1,
                          width: h1,
                        ),
                        Spacer(),
                        Text(
                          'Ranger Jane Freecs',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: tittle,
                            color: backc,
                          ),
                        ),
                        Spacer(),
                        Text(
                          'SKills: Foundation First-Aid,',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: text,
                            color: backc,
                          ),
                        ),
                        Spacer(),
                        Text(
                          'Signal For Help',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: text,
                            color: backc,
                          ),
                        ),
                        Spacer(),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.amber,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Schedule_page(),
                              ),
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 25, right: 25, top: 15, bottom: 15),
                            child: Text(
                              'Select',
                              style: TextStyle(
                                  fontSize: text,
                                  color: backc,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20), color: textc),
                    height: h3,
                  ),
                ),
              ],
            ),
          ),
          Container(
            color: textc,
            height: MediaQuery.of(context).size.height * .07,
            child: Row(
              children: [
                Spacer(),
                IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Dashboard_page(),
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.home,
                    size: tittle,
                    color: backc,
                  ),
                ),
                Spacer(),
                Spacer(),
                Spacer(),
                IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Profile_page(),
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.person,
                    size: tittle,
                    color: backc,
                  ),
                ),
                Spacer(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
