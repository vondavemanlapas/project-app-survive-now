import 'package:flutter/material.dart';
import 'package:survive_now/profile.dart';
import 'package:survive_now/dashboard.dart';
import 'package:survive_now/schedule.dart';

class Schedule_page extends StatefulWidget {
  const Schedule_page({super.key});

  @override
  State<Schedule_page> createState() => _Schedule_pageState();
}

class _Schedule_pageState extends State<Schedule_page> {
  DateTime selectedDate = DateTime.now();
  @override
  Widget build(BuildContext context) {
    double h1 = MediaQuery.of(context).size.height * .2;
    double h2 = MediaQuery.of(context).size.height * .1;
    double h3 = MediaQuery.of(context).size.height * .4;
    double tittle = MediaQuery.of(context).size.height * .05;
    double text = MediaQuery.of(context).size.height * .03;
    Color textc = Colors.green;
    Color backc = Color.fromARGB(255, 242, 240, 236);
    return Scaffold(
      backgroundColor: backc,
      body: Column(
        children: [
          Spacer(),
          Icon(
            Icons.sports_kabaddi,
            size: tittle,
          ),
          Text(
            'Choose Schedule',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: tittle,
              color: Colors.green,
            ),
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.green.withOpacity(.7),
              ),
              child: CalendarDatePicker(
                  initialDate: selectedDate,
                  firstDate: DateTime(2022),
                  lastDate: DateTime(2025),
                  onDateChanged: (DateTime? value) {
                    if (value != null) {
                      setState(() {
                        selectedDate = value;
                      });
                    }
                  }),
              height: h3,
            ),
          ),
          Spacer(),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: textc,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50),
              ),
            ),
            onPressed: () {
              final snackBar = SnackBar(
                content: Text("Submitted, Schedule is on $selectedDate"),
                action: SnackBarAction(
                    label: "OK",
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Dashboard_page(),
                        ),
                      );
                    }),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            },
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 25, right: 25, top: 15, bottom: 15),
              child: Text(
                'Submit',
                style: TextStyle(fontSize: text, color: backc),
              ),
            ),
          ),
          Spacer(),
          Container(
            color: textc,
            height: MediaQuery.of(context).size.height * .07,
            child: Row(
              children: [
                Spacer(),
                IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Dashboard_page(),
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.home,
                    size: tittle,
                    color: backc,
                  ),
                ),
                Spacer(),
                Spacer(),
                Spacer(),
                IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Profile_page(),
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.person,
                    size: tittle,
                    color: backc,
                  ),
                ),
                Spacer(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
