import 'dart:math';

import 'package:flutter/material.dart';
import 'package:survive_now/log.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Log_page(),
    );
  }
}