import 'package:flutter/material.dart';
import 'package:survive_now/profile.dart';
import 'package:survive_now/dashboard.dart';
import 'package:survive_now/service.dart';

class Dashboard_page extends StatelessWidget {
  const Dashboard_page({super.key});

  @override
  Widget build(BuildContext context) {
    double h1 = MediaQuery.of(context).size.height * .2;
    double h2 = MediaQuery.of(context).size.height * .1;
    double tittle = MediaQuery.of(context).size.height * .05;
    double text = MediaQuery.of(context).size.height * .03;
    Color textc = Colors.green;
    Color backc = Color.fromARGB(255, 242, 240, 236);
    return Scaffold(
      backgroundColor: backc,
      body: Column(
        children: [
          Spacer(),
          Icon(
            Icons.sports_kabaddi,
            size: tittle,
          ),
          Text(
            'Choose Environment',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: tittle,
              color: Colors.green,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                elevation: 0,
                backgroundColor: Colors.transparent,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Service_page(),
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  image: DecorationImage(
                      image: AssetImage('assets/forest.jpg'),
                      fit: BoxFit.cover),
                ),
                height: h1,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                elevation: 0,
                backgroundColor: Colors.transparent,
              ),
              onPressed: () {},
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  image: DecorationImage(
                      image: AssetImage('assets/desert.jpg'),
                      fit: BoxFit.cover),
                ),
                height: h1,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                elevation: 0,
                backgroundColor: Colors.transparent,
              ),
              onPressed: () {},
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  image: DecorationImage(
                      image: AssetImage('assets/abandoned.jpg'),
                      fit: BoxFit.cover),
                ),
                height: h1,
              ),
            ),
          ),
          Spacer(),
          Container(
            color: textc,
            height: MediaQuery.of(context).size.height * .07,
            child: Row(
              children: [
                Spacer(),
                IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Dashboard_page(),
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.home,
                    size: tittle,
                    color: backc,
                  ),
                ),
                Spacer(),
                Spacer(),
                Spacer(),
                IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Profile_page(),
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.person,
                    size: tittle,
                    color: backc,
                  ),
                ),
                Spacer(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
