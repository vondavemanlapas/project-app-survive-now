import 'package:flutter/material.dart';
import 'package:survive_now/log.dart';

class Profile_page extends StatelessWidget {
  const Profile_page({super.key});

  @override
  Widget build(BuildContext context) {
    double h1 = MediaQuery.of(context).size.height * .2;
    double h3 = MediaQuery.of(context).size.height * .45;
    double h2 = MediaQuery.of(context).size.height * .1;
    double tittle = MediaQuery.of(context).size.height * .05;
    double text = MediaQuery.of(context).size.height * .03;
    Color textc = Colors.green;
    Color backc = Color.fromARGB(255, 242, 240, 236);
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(8),
        child: Column(
          children: [
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.sports_kabaddi,
                  size: tittle,
                ),
                Text(
                  'Survive Now',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: text,
                      color: textc),
                ),
              ],
            ),
            Icon(
              Icons.person,
              size: h1,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Von Dave Manlapas',
              style: TextStyle(fontSize: text),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Spacer(),
                Expanded(
                  flex: 4,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50),
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Log_page(),
                        ),
                      );
                    },
                    child: Text(
                      'Update',
                      style: TextStyle(fontSize: text, color: Colors.black),
                    ),
                  ),
                ),
                Spacer(),
              ],
            ),
            Row(
              children: [
                Spacer(),
                Expanded(
                  flex: 4,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50),
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Log_page(),
                        ),
                      );
                    },
                    child: Text(
                      'Delete Account',
                      style: TextStyle(fontSize: text, color: Colors.black),
                    ),
                  ),
                ),
                Spacer(),
              ],
            ),
            Row(
              children: [
                Spacer(),
                Expanded(
                  flex: 4,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.grey,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50),
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Log_page(),
                        ),
                      );
                    },
                    child: Text(
                      'Log Out',
                      style: TextStyle(fontSize: text, color: Colors.black),
                    ),
                  ),
                ),
                Spacer(),
              ],
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
