import 'package:flutter/material.dart';
import 'package:survive_now/dashboard.dart';

class Log_page extends StatefulWidget {
  const Log_page({super.key});

  @override
  State<Log_page> createState() => _Log_pageState();
}

class _Log_pageState extends State<Log_page> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    double tittle = MediaQuery.of(context).size.height * .05;
    double text = MediaQuery.of(context).size.height * .03;
    Color textc = Colors.green;
    Color backc = Color.fromARGB(255, 242, 240, 236);
    return Scaffold(
      body: SafeArea(
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints viewportConstraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: viewportConstraints.maxHeight,
                ),
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: Column(
                    children: [
                      Container(
                        child: Column(
                          children: [
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .05,
                            ),
                            Icon(
                              Icons.sports_kabaddi,
                              size: tittle,
                            ),
                            Text(
                              'Survive Now',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: tittle,
                                color: Colors.green,
                              ),
                            ),
                            Text(
                              'Technical Training Skills',
                              style: TextStyle(fontSize: text),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * .1,
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * .48,
                        child: Form(
                          key: _formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Spacer(),
                              Text(
                                'Username:',
                                style: TextStyle(fontSize: text),
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: backc,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15),
                                      borderSide: BorderSide.none),
                                  suffixIcon: Icon(
                                    Icons.person,
                                    color: Colors.black,
                                  ),
                                  hintText: 'Enter Username',
                                  hintStyle: TextStyle(color: textc),
                                ),
                                validator: (String? value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please Enter Username';
                                  }
                                  return null;
                                },
                              ),
                              Spacer(),
                              Text(
                                'Password',
                                style: TextStyle(fontSize: text),
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: backc,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15),
                                      borderSide: BorderSide.none),
                                  suffixIcon: Icon(
                                    Icons.remove_red_eye_outlined,
                                    color: Colors.black,
                                  ),
                                  hintText: 'Enter Password',
                                  hintStyle: TextStyle(color: textc),
                                ),
                                validator: (String? value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please Enter Password';
                                  }
                                  return null;
                                },
                              ),
                              Expanded(
                                child: Row(
                                  children: [
                                    Spacer(),
                                    Text('Forget Password'),
                                  ],
                                ),
                              ),
                              Spacer(),
                              Center(
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: textc,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                  ),
                                  onPressed: () {
                                    if (_formKey.currentState!.validate()) {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              Dashboard_page(),
                                        ),
                                      );
                                    }
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 25,
                                        right: 25,
                                        top: 15,
                                        bottom: 15),
                                    child: Text(
                                      'Log In',
                                      style: TextStyle(
                                          fontSize: text, color: backc),
                                    ),
                                  ),
                                ),
                              ),
                              Spacer(),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
